// DO NOT EDIT: auto-generated with `pub run custom_element_apigen:update`

/// Dart API for the polymer element `the_graph`.
library the_graph.the_graph;

import 'dart:html';
import 'dart:js' show JsArray, JsObject;
import 'package:web_components/interop.dart' show registerDartType;
import 'package:polymer/polymer.dart' show initMethod;
import 'package:custom_element_apigen/src/common.dart' show PolymerProxyMixin, DomProxyMixin;


class TheGraph extends HtmlElement with DomProxyMixin, PolymerProxyMixin {
  TheGraph.created() : super.created();
  factory TheGraph() => new Element.tag('the-graph');

  get graph => jsElement[r'graph'];
  set graph(value) { jsElement[r'graph'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get menus => jsElement[r'menus'];
  set menus(value) { jsElement[r'menus'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get library => jsElement[r'library'];
  set library(value) { jsElement[r'library'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get width => jsElement[r'width'];
  set width(value) { jsElement[r'width'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get height => jsElement[r'height'];
  set height(value) { jsElement[r'height'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get autolayout => jsElement[r'autolayout'];
  set autolayout(value) { jsElement[r'autolayout'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get theme => jsElement[r'theme'];
  set theme(value) { jsElement[r'theme'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get selectedNodes => jsElement[r'selectedNodes'];
  set selectedNodes(value) { jsElement[r'selectedNodes'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get errorNodes => jsElement[r'errorNodes'];
  set errorNodes(value) { jsElement[r'errorNodes'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get selectedEdges => jsElement[r'selectedEdges'];
  set selectedEdges(value) { jsElement[r'selectedEdges'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get animatedEdges => jsElement[r'animatedEdges'];
  set animatedEdges(value) { jsElement[r'animatedEdges'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get getMenuDef => jsElement[r'getMenuDef'];
  set getMenuDef(value) { jsElement[r'getMenuDef'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get pan => jsElement[r'pan'];
  set pan(value) { jsElement[r'pan'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get scale => jsElement[r'scale'];
  set scale(value) { jsElement[r'scale'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get displaySelectionGroup => jsElement[r'displaySelectionGroup'];
  set displaySelectionGroup(value) { jsElement[r'displaySelectionGroup'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get forceSelection => jsElement[r'forceSelection'];
  set forceSelection(value) { jsElement[r'forceSelection'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get offsetY => jsElement[r'offsetY'];
  set offsetY(value) { jsElement[r'offsetY'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}

  get offsetX => jsElement[r'offsetX'];
  set offsetX(value) { jsElement[r'offsetX'] = (value is Map || value is Iterable) ? new JsObject.jsify(value) : value;}
}
@initMethod
upgradeTheGraph() => registerDartType('the-graph', TheGraph);
