# Changelog

## 0.0.8+5

- Fixed ports info issue
- Made port names case sensitive 

## 0.0.8+3

- Added registerComponent method

## 0.0.8+2

- Added note edition
- Made notes selectable

## 0.0.8+1

- Fixed notes size

## 0.0.8

- Added notes provide

## 0.0.7

- Added experimental zooming feature

## 0.0.6

- Fixed x,y offsets in mouse events

## 0.0.3

- Added pan, offsetX, offsetY, scale properties and focusNode method.

## 0.0.1

- Initial version, created by Stagehand
